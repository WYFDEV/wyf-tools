# coding=utf-8

import subprocess
import os

from .. import cmd
from .. import const

cmd.sub('sslocal', aliases=['ss'], help='start sslocal')


@cmd.main('sslocal')
def main(args):
    pid = os.fork()

    if pid == 0:
        config_dir = os.path.expanduser(const.config.dir)
        os.chdir(config_dir)
        cmd_sslocal = [
            'sslocal', '-c',
            os.path.join(config_dir, 'shadowsocks.json')
        ]
        cmd_polipo = [
            'polipo', '-c',
            os.path.join(config_dir, 'polipo.conf')
        ]

        DEVNULL = subprocess.DEVNULL
        subprocess.Popen(cmd_sslocal, stdout=DEVNULL, stderr=DEVNULL)
        subprocess.Popen(cmd_polipo, stdout=DEVNULL, stderr=DEVNULL)
    else:
        print('sslocal & polipo start success')
