# coding=utf-8

import os
import subprocess

from .. import cmd
from .. import const

editor = os.environ.get('EDITOR', const.default_editor or 'vim')


cmd.sub('config', help='edit config file')


@cmd.main('config')
def main(args):
    conf_dir = os.path.expanduser(const.config.dir)
    files = sorted(os.listdir(conf_dir))
    print('choise config file:')
    # list files
    for i in range(len(files)):
        print('  [{}]  {}'.format(i + 1, files[i]))
    # choise file
    while True:
        try:
            index = int(input('>> ')) - 1
            if index < 0:
                raise IndexError
            file = files[index]
        except (IndexError, ValueError):
            pass
        else:
            print('\nedit {}'.format(file))
            break
    conf_file = os.path.join(conf_dir, file)
    subprocess.call([editor, conf_file])
