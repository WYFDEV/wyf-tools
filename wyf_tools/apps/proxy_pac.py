# coding=utf-8

import socket
from http import server

from .. import cmd


@cmd.sub('proxy_pac', aliases=['pac'], help='start a simple proxy pac server')
def init(parser):
    # parser.add_argument('-h', '--host', help='local host')
    # parser.add_argument('-p', '--port', type=int, help='local port')
    parser.add_argument('query', nargs='*', help='text to query')


class Handler(server.BaseHTTPRequestHandler):
    def do_GET(self):
        if self.path != '/':
            self.send_error(404, "File not found.")
        # status
        self.send_response(200)
        # heads
        # content_type = 'application/x-javascript-config'
        content_type = 'application/x-ns-proxy-autoconfig'
        self.send_header('Content-type', content_type)
        self.end_headers()
        # body
        try:
            self_ip = socket.gethostbyname(socket.gethostname())
        except Exception as err:
            raise
        pac_txt = '''
        function FindProxyForURL(url, host){
            return "PROXY {ip}:{port}; DIRECT";
        }
        '''.format(ip=self_ip, port=9001)
        # write response
        self.wfile.write(pac_txt.encode('utf-8'))


@cmd.main('proxy_pac')
def main(args):
    address = ('0.0.0.0', 9002)
    try:
        httpd = server.HTTPServer(address, Handler)
        print('Server listening {}:{}'.format(*address))
        httpd.serve_forever()
    except KeyboardInterrupt:
        print('End server')
