# coding=utf-8

import sys
import os.path
import requests
from urllib.parse import urlparse

from .. import cmd
from .. import conf

debug = False


@cmd.sub('proxy_download', aliases=['pd'], help='downlod file use proxy')
def init(parser):
    parser.add_argument('-o', '--output', default=None, help='output path')
    parser.add_argument('-debug', action='store_true', help='debug mode')
    parser.add_argument('url', nargs=1, help='URL to download')


@cmd.main('proxy_download')
def main(args):
    global debug
    debug = args.debug

    proxies = conf.proxies

    url = args.url[0]
    file_name = args.output or os.path.basename(urlparse(url).path)

    with open(file_name, "wb") as f:
        print('Downloading {}'.format(file_name))
        res = requests.get(url, proxies=proxies, stream=True)
        total_length = res.headers.get('content-length')

        if total_length is None:  # no content length header
            f.write(res.content)
        else:
            done = 0
            total_length = int(total_length)
            for data in res.iter_content(chunk_size=4096):
                done += len(data)
                f.write(data)
                percentage = int(100 * done / total_length)
                done_range = int(50 * done / total_length)
                sys.stdout.write('\r[{}{}] {:3}%'
                                 .format('=' * done_range,
                                         ' ' * (50 - done_range),
                                         percentage))
                sys.stdout.flush()
    print()
