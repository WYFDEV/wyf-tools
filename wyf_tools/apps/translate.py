# coding=utf-8

import os
import sys
import html
import requests
import subprocess
import tempfile

from .. import cmd
from .. import conf
from .. import const

debug = False
editor = os.environ.get('EDITOR', const.default_editor or 'vim')


@cmd.sub('translate', aliases=['tr'], help='translate')
def init(parser):
    parser.add_argument('-s', '--source', help='source language')
    parser.add_argument('-t', '--target', help='target language')
    parser.add_argument('-d', '--detect', help='detect language')
    parser.add_argument('-e', '--edit', action='store_true',
                        default=False, help='enter edit mode')
    parser.add_argument('-v', '--view', action='store_true',
                        default=False, help='view history')
    parser.add_argument('-S', '--skip-recored',
                        action="store_true", default=False, help='skip save')
    parser.add_argument('--not-echo-query', action="store_true",
                        default=False, help='not echo query')
    parser.add_argument('query', nargs='*', help='text to query')
    parser.add_argument('--debug', action='store_true', help='debug moduel')


@cmd.main('translate')
def main(args):
    global debug
    debug = args.debug
    echo_query = not args.not_echo_query

    if args.view:
        view_history()
        return

    if args.edit:
        query = edit_query()
    else:
        query = ' '.join(args.query)

    if not query:
        query = sys.stdin.read()
        echo_query = False
    if not query:
        raise cmd.ArgumentError('query empty')

    if args.detect:
        result = detect(query)
        print(result)
        return

    dft_src, dft_tar = (
        'zh-cn', 'en') if is_chinese(query) else ('en', 'zh-cn')
    source = args.source or dft_src
    target = args.target or dft_tar

    try:
        result = translate(query, source, target)
    except requests.exceptions.ConnectionError:
        if debug:
            raise
        sys.exit('Please check network')
    except:
        if debug:
            raise
        sys.exit('Some error')

    if echo_query:
        print(query)
    print(result)

    if not args.skip_recored:
        recored_history(query, result, source, target)
    print()


def translate(query, source, target):
    api_uri = const.transalate.transalate_api
    api_params = {
        'key': conf.gcloud_api_key,
        'source': source,
        'target': target,
        'q': query
    }
    proxies = conf.proxies if conf.transalate.use_proxies else None
    res = requests.get(api_uri, params=api_params, proxies=proxies)
    if debug:
        print(res.text)
    res = res.json()

    if 'error' in res:
        return 'Translate Failed'
    else:
        return html.unescape(' '.join([
            val.get('translatedText', '')
            for val in res['data']['translations']
        ]))


def speak(text):
    pass


def detect(query):
    api_uri = const.transalate.detect_api
    api_params = {'key': conf.gcloud_api_key, 'q': query}
    proxies = conf.proxies if conf.transalate.use_proxies else None
    res = requests.get(api_uri, params=api_params, proxies=proxies)
    res = res.json()

    if 'error' in res:
        return False
    else:
        return sorted(res['data']['detections'][0], key=lambda i: float(i['confidence']))


def is_chinese(string):
    return any('\u4e00' <= c <= '\u9fff' for c in string)


def view_history():
    save_as = conf.transalate.save_as
    if save_as == 'file':
        Recorder = FileRecorder
    elif save_as == 'db':
        Recorder = DBRecorder
    else:
        sys.stderr.write(
            'Unknow type of "transalate.save_as" : {}'.format(save_as))
    recoreder = Recorder(conf.transalate.save_to)
    recoreder.view()


def edit_query():
    with tempfile.NamedTemporaryFile(suffix=".txt") as f:
        subprocess.call([editor, f.name])
        f.seek(0)
        return f.read().decode()


def recored_history(query, result, source=None, target=None):
    if not conf.transalate.save_to or not conf.transalate.save_as:
        return

    save_as = conf.transalate.save_as
    if save_as == 'file':
        Recorder = FileRecorder
    elif save_as == 'db':
        Recorder = DBRecorder
    else:
        sys.stderr.write(
            'Unknow type of "transalate.save_as" : {}'.format(save_as))
        return
    recoreder = Recorder(conf.transalate.save_to)
    recoreder.save(query, result, source, target)


class FileRecorder:
    def __init__(self, file):
        self.file = os.path.expanduser(file)

    def save(self, query, result, *_):
        with open(self.file, 'a') as f:
            f.write('\n')
            f.write(query)
            f.write('\n')
            f.write(result)
            f.write('\n')

    def view(self):
        if not self.file:
            return
        subprocess.call([edit, os.path.expanduser(conf.transalate.save_to)])


class DBRecorder():
    def __init__(self, file):
        import sqlite3
        file = os.path.expanduser(file)
        self.conn = sqlite3.connect(file)
        self.c = self.conn.cursor()
        self.create_table()

    def create_table(self):
        self.c.execute('CREATE TABLE IF NOT EXISTS translate_log ('
                       '`query` TEXT, `result` TEXT,'
                       '`source` CHARACTER(8), `target` CHARACTER(8),'
                       '`time` TIMESTAMP DEFAULT CURRENT_TIMESTAMP)')
        self.conn.commit()

    def save(self, query, result, source, target):
        if query == result:
            return
        self.c.execute('INSERT INTO translate_log'
                       ' (`query`, `result`, `source`, `target`)'
                       ' VALUES (?, ?, ?, ?)',
                       (query, result, source or None, target or None))
        self.conn.commit()

    def view(self):
        self.c.execute('SELECT query, result, time FROM translate_log')
        with tempfile.NamedTemporaryFile(suffix=".txt", delete=False) as f:
            for row in self.c:
                f.write('{}\n{}\n{}\n\n'.format(*row).encode('UTF-8'))
        subprocess.call([editor, f.name])

    def __del__(self):
        self.c.close()
        self.conn.close()
