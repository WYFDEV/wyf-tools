# coding=utf-8

# setting google domain ddns by api
# see:
#   https://support.google.com/domains/answer/6147083?hl=zh-Hant

import requests
import socket
import sys

from .. import cmd
from .. import conf
from .. import __version__

debug = False


@cmd.sub('gddns', help='update google domain ddns')
def init(parser):
    parser.add_argument('hostname', help='hostname')
    parser.add_argument('ip', help='ip address want set')
    parser.add_argument('--offline', action='store_true', default=False,
                        help='sets the current host to offline status')


@cmd.main('gddns')
def main(args):
    hostname = args.hostname
    ip_addr = args.ip
    offline = 'yes' if args.offline else 'no'

    try:
        hostconf = conf.google_domains_ddns[hostname]
        if None in (hostconf.username, hostconf.password):
            raise('Domain "{}" not configed success'.format(hostname))
        socket.inet_aton(ip_addr)
    except KeyError as e:
        sys.exit('Domain "{}" not configed'.format(e.args[0]))
    except socket.error:
        sys.exit('Ip address "{}" invalid'.format(ip_addr))

    url = 'https://domains.google.com/nic/update'.format(
        hostconf.username, hostconf.password)
    params = {'hostname': hostname, 'myip': ip_addr, 'offline': offline}

    print('hostname:\t{}'.format(hostname))
    print('ip address:\t{}'.format(ip_addr))
    print('offline:\t{}'.format(offline))

    if input('Sure? [Y/n] : ').lower() not in ('y', 'yes'):
        sys.exit()

    try:
        resp = requests.get(
            url,
            auth=(hostconf.username, hostconf.password),
            params=params,
            headers={'User-Agent': 'wyf_tools/{}'.format(__version__)},
            timeout=15
        )
        print()
        print(resp.text)
        print()
    except requests.exceptions.ConnectionError:
        sys.exit('Please check network')
    except:
        raise


cmd.sub('getip', help='get my ip address')

@cmd.main('getip')
def get_my_ip(args):
    resp = requests.get('https://domains.google.com/checkip',
                        headers={
                            'User-Agent': 'wyf_tools/{}'.format(__version__)},
                        timeout=15)
    print('My ip:')
    print('\t{}'.format(resp.text))
    return resp.text

