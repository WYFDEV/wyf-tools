# coding=utf-8

__version__ = '0.3.1'

__all__ = ['main', 'const', 'conf', 'configer', 'cmd']

from . import cmd
from .main import main
from .conf import configer

const = None
conf = None


def init():
    '''init'''
    import sys, shutil, os, os.path as path
    global const, conf
    basedir = path.dirname(path.abspath(__file__))
    # load const
    const = configer(path.join(basedir, 'const.yaml'), {'basedir': basedir})
    # init user envriment
    config_dir = path.expanduser(const.config.dir)
    if not os.path.exists(config_dir):
        print('Tools init\n'
              'config files at:\n'
              '    {}'.format(const.config.dir))
        print('use "{} config" edit it'.format(sys.argv[0]))
        dft_conf = os.path.join(const.basedir, 'config')
        shutil.copytree(dft_conf, config_dir)
        sys.exit(0)
    # load conf
    config_main = path.join(config_dir, const.config.main)
    conf = configer(config_main)


init()

