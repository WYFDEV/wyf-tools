# coding=utf-8

import sys
import pkg_resources
from .cmd import execute


def main():
    '''entry point'''
    for entry_point in pkg_resources.iter_entry_points('wyf_tools_app'):
        try:
            entry_point.load()
        except Exception:
            print(f'Error when load app "{entry_point.name}"')
            sys.exit(1)
    try:
        execute()
    except KeyboardInterrupt:
        print()
