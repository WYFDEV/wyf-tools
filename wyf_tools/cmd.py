# coding=utf-8

import argparse
import functools

from . import __version__

parser = argparse.ArgumentParser(description='wyf_tools')
parser.add_argument('--version', action='version', version='%(prog)s {}'.format(__version__))
subparser = parser.add_subparsers()
subparsers = dict()


class ArgumentError(Exception):
    pass


def sub(name, *args, **karg):
    if name not in subparsers:
        parser = subparser.add_parser(name, *args, **karg)
        parser.set_defaults(_parser=parser)
        subparsers[name] = parser
    def warpper(func):
        func(parser)
    return warpper


def main(name):
    def warpper(func):
        subparsers[name].set_defaults(_func=func)
        @functools.wraps(func)
        def warp():
            args = subparsers[name].parse_args()
            func(args)
        return warp
    return warpper


def execute():
    args = parser.parse_args()
    if hasattr(args, '_func'):
        try:
            args._func(args)
        except ArgumentError as err:
            print(' '.join(err.args), '\n')
            args._parser.print_usage()
            args._parser.exit(1)
    else:
        parser.print_usage()

