import yaml


def configer(file, data_default=None, data_update=None, update_func=None):
    '''get config from yaml
    Args:
        file (str)          : yaml file path
        date_default (dict) : dict default value
        date_update  (dict) : dict update value
        update_func  (callable) : func update data, receive one argument data
    '''
    config = yaml.load(open(file)) or dict()
    if isinstance(data_default, dict):
        for k, v in data_default.items():
            config.setdefault(k, v)
    if isinstance(data_update, dict):
        config.update(data_update)
    if callable(update_func):
        update_func(config)
    return Config(config)


class Config():
    def __init__(self, data):
        self._data = data

    def _wrap(self, val):
        if isinstance(val, (dict, list)):
            return self.__class__(val)
        return val

    def __getattr__(self, key):
        return self._wrap(self._data.get(key, None)) or \
            getattr(self._data, key, None)

    def __getitem__(self, index):
        return self._wrap(self._data[index])

    def __iter__(self):
        for item in self._data:
            yield self._wrap(self.__class__(item))

    def __repr__(self):
        return '<Config Object: {}>'.format(self._data)

