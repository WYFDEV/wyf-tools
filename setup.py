from setuptools import setup

from wyf_tools import __version__

setup(
    name        = 'wyf-tools',
    author      = 'WYF',
    author_email= 'dev@wyf.io',
    version     = __version__,
    packages    = ['wyf_tools', 'wyf_tools.apps'],
    url         = 'http://project.wyf.io/tools',
    license     = 'MIT',
    description = 'some tools',
    long_description = open('./README.md').read(),
    python_requires  = '>=3',
    install_requires = [
        'PyYAML',
        'requests',
    ],
    package_data  = {
        'wyf_tools': ['*.yaml', 'config/*']
    },
    entry_points = {
        'console_scripts': [
            'wyf-tools=wyf_tools.__main__:main'
        ],
        'wyf_tools_app': [
            'config=wyf_tools.apps.config',
            'gddns=wyf_tools.apps.gddns',
            'proxy_download=wyf_tools.apps.proxy_download',
            'proxy_pac=wyf_tools.apps.proxy_pac',
            'sslocal=wyf_tools.apps.sslocal',
            'translate=wyf_tools.apps.translate'
        ]
    }
)
