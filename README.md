# Tools

## Description
This is my personal commandline toolset

## Install
```
python setup.py install
```

or

```
pip install .
```

## Usage
```
wyf-tools --help
```
